import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: string[];
  constructor() {
    this.destinos = ['Cartagena','Lima','Barcelona','Buenos Aires','Medellin'];
  }

  ngOnInit(): void {
  }
  agregar(titulo: HTMLInputElement)	{
    console.log(titulo.value);
    }
}
